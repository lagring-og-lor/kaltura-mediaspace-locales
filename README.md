# Kaltura MediaSpace Locales

Nynorsk og bokmål oversettelser av Klatura MediaSpace. 

## Branchstrategi

* `Master`: siste versjon av `Norwegian` merget etter rebase på `Original` dersom det har vært oppdateringer
* `Original`: språkpakke lastet ned fra Kaltura KAF
* `Norwegian`: norskoversettelse
